//
// Created by ruben.laso on 16/11/22.
//

#ifndef RETOCHOEU_1_WINDMILL_HPP
#define RETOCHOEU_1_WINDMILL_HPP

#include <string>

#include <armadillo>
#include <ostream>
#include <utility>

#include "Point.hpp"

class Windmill : public Point
{
private:
	std::string name_{};

	size_t id_{};

	double blades_speed_limit_{};  // km/h
	double sensor_latency_{};      // ms
	double blades_{};              //
	double blades_length_{};       // m
	double speed_average_{};       // km/h
	double power_{};               // MW
	double sensor_accuracy_{};     // [0, 100] %
	double sensor_blades_speed_{}; // km/h

public:
	Windmill() = default;

	Windmill(const Point & coordinates, const std::string & name, double blades_speed_limit, double sensor_latency,
	         double blades, double blades_length, double speed_average, double power, double sensor_accuracy,
	         double sensor_blades_speed) :
	    Point(coordinates),
	    name_(name),
	    id_(std::strtol(split_line(name)[1].c_str(), nullptr, 10)),
	    blades_speed_limit_(blades_speed_limit),
	    sensor_latency_(sensor_latency),
	    blades_(blades),
	    blades_length_(blades_length),
	    speed_average_(speed_average),
	    power_(power),
	    sensor_accuracy_(sensor_accuracy),
	    sensor_blades_speed_(sensor_blades_speed)
	{}

	Windmill(const double x, const double y, const std::string & name, double blades_speed_limit, double sensor_latency,
	         double blades, double blades_length, double speed_average, double power, double sensor_accuracy,
	         double sensor_blades_speed) :
	    Point({ x, y }),
	    name_(name),
	    id_(std::strtol(split_line(name)[1].c_str(), nullptr, 10)),
	    blades_speed_limit_(blades_speed_limit),
	    sensor_latency_(sensor_latency),
	    blades_(blades),
	    blades_length_(blades_length),
	    speed_average_(speed_average),
	    power_(power),
	    sensor_accuracy_(sensor_accuracy),
	    sensor_blades_speed_(sensor_blades_speed)
	{}

	[[nodiscard]] inline auto coordinates() const -> arma::rowvec2 { return arma::rowvec2{ getX(), getY() }; }

	inline void lin_comb_x(const arma::rowvec5 & lin_comb) { coeffs_x(lin_comb); }
	inline void lin_comb_y(const arma::rowvec5 & lin_comb) { coeffs_y(lin_comb); }

	[[nodiscard]] inline auto name() const -> const std::string & { return name_; }

	void name(const std::string & name) { name_ = name; }

	[[nodiscard]] inline auto id() const { return id_; }

	[[nodiscard]] inline auto blades_speed_limit() const { return blades_speed_limit_; }

	inline void blades_speed_limit(double blades_speed_limit) { blades_speed_limit_ = blades_speed_limit; }

	[[nodiscard]] inline auto sensor_latency() const { return sensor_latency_; }

	void sensor_latency(double sensor_latency) { sensor_latency_ = sensor_latency; }

	[[nodiscard]] inline auto blades() const { return blades_; }

	inline void blades(double blades) { blades_ = blades; }

	[[nodiscard]] inline auto blades_length() const { return blades_length_; }

	inline void blades_length(double blades_length) { blades_length_ = blades_length; }

	[[nodiscard]] inline auto speed_average() const { return speed_average_; }

	inline void speed_average(double speed_average) { speed_average_ = speed_average; }

	[[nodiscard]] inline auto power() const { return power_; }
	inline void               power(double power) { power_ = power; }

	[[nodiscard]] inline auto sensor_accuracy() const { return sensor_accuracy_; }

	inline void sensor_accuracy(double sensor_accuracy) { sensor_accuracy_ = sensor_accuracy; }

	[[nodiscard]] inline auto sensor_blades_speed() const { return sensor_blades_speed_; }

	inline void sensor_blades_speed(double sensor_blades_speed) { sensor_blades_speed_ = sensor_blades_speed; }

	friend auto operator<<(std::ostream & os, const Windmill & windmill) -> std::ostream &
	{
		os << "coordinates: [" << windmill.getX() << ", " << windmill.getY() << "], " //
		   << "name: " << windmill.name_ << ", "                                      //
		   << "id: " << windmill.id_ << ", "                                          //
		   << "blades_speed_limit: " << windmill.blades_speed_limit_ << ", "          //
		   << "sensor_latency: " << windmill.sensor_latency_ << ", "                  //
		   << "blades: " << windmill.blades_ << ", "                                  //
		   << "blades_length: " << windmill.blades_length_ << ", "                    //
		   << "speed_average: " << windmill.speed_average_ << ", "                    //
		   << "power: " << windmill.power_ << ", "                                    //
		   << "sensor_accuracy: " << windmill.sensor_accuracy_ << ", "                //
		   << "sensor_blades_speed: " << windmill.sensor_blades_speed_;
		return os;
	}
};

#endif /* end of include guard: RETOCHOEU_1_WINDMILL_HPP */
