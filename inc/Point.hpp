#pragma once
//
// Created by ruben.laso on 21/11/22.
//

#ifndef RETOCHOEU_1_POINT_HPP
#define RETOCHOEU_1_POINT_HPP

#include <armadillo>

#include "utils.hpp"

class Point : public arma::rowvec2
{
	arma::rowvec5 coeffs_x_;
	arma::rowvec5 coeffs_y_;

public:
	Point() = default;
	Point(const arma::rowvec2 & vec) : arma::rowvec2(vec){};
	Point(const arma::rowvec2 & vec, const arma::rowvec5 & coeffs_x, const arma::rowvec5 & coeffs_y) :
	    arma::rowvec2(vec), coeffs_x_(coeffs_x), coeffs_y_(coeffs_y){};

	// template<typename... T>
	// Point(T... initializer_list) : arma::rowvec2(initializer_list...){};

	[[nodiscard]] inline auto getX() const { return this->operator[](0); }
	[[nodiscard]] inline auto getY() const { return this->operator[](1); }

	[[nodiscard]] inline auto coeffs_x() const { return coeffs_x_; }
	[[nodiscard]] inline auto coeffs_y() const { return coeffs_y_; }

	inline void coeffs_x(const arma::rowvec5 & coeffs) { coeffs_x_ = coeffs; }
	inline void coeffs_y(const arma::rowvec5 & coeffs) { coeffs_y_ = coeffs; }

	[[nodiscard]] static inline auto distance(const Point & p1, const Point & p2)
	{
		const auto diff_x = p1.getX() - p2.getX();
		const auto diff_y = p1.getY() - p2.getY();
		return std::sqrt(square(diff_x) + square(diff_y));
	}
};

using Vector = Point;

#endif /* end of include guard: RETOCHOEU_1_POINT_HPP */
