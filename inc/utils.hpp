//
// Created by ruben.laso on 17/11/22.
//

#ifndef RETOCHOEU_1_UTILS_HPP
#define RETOCHOEU_1_UTILS_HPP

#include <sstream>
#include <string>
#include <vector>

template<typename T>
constexpr inline auto square(const T & n) -> T
{
	return n * n;
}

template<typename T>
constexpr inline auto in_interval(const T & n, const T & min, const T & max) -> bool
{
	return n >= min && n <= max;
}

[[nodiscard]] inline auto split_line(const std::string & line, const char separator = ' ') -> std::vector<std::string>
{
	std::stringstream        stream{ line };
	std::string              segment;
	std::vector<std::string> segments;

	while (std::getline(stream, segment, separator))
	{
		segments.push_back(segment);
	}

	return segments;
}

#endif /* end of include guard: RETOCHOEU_1_UTILS_HPP */
