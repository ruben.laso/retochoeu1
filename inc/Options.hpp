#pragma once
//
// Created by ruben.laso on 17/11/22.
//

#ifndef RETOCHOEU_1_OPTIONS_HPP
#define RETOCHOEU_1_OPTIONS_HPP

#include <filesystem>
#include <string>
#include <vector>

class Options
{
public:
	static constexpr bool   DEFAULT_VERBOSE   = false;
	static constexpr bool   DEFAULT_DEBUG     = false;
	static constexpr size_t DEFAULT_RANGE     = 10;
	static constexpr double DEFAULT_TOLERANCE = 5e-5;

	bool   verbose   = DEFAULT_VERBOSE;
	bool   debug     = DEFAULT_DEBUG;
	size_t range     = DEFAULT_RANGE;
	double tolerance = DEFAULT_TOLERANCE;

	std::vector<std::filesystem::path> files;
};

extern Options options;

#endif /* end of include guard: RETOCHOEU_1_OPTIONS_HPP */
