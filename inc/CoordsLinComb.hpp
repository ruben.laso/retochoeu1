//
// Created by ruben.laso on 17/11/22.
//

#ifndef RETOCHOEU_1_COORDSLINCOMB_HPP
#define RETOCHOEU_1_COORDSLINCOMB_HPP

#include <ranges>

#include <armadillo>

#include "Options.hpp"
#include "Point.hpp"
#include "Windmill.hpp"

namespace coords
{
	static const Point Leon({ -5.59698, 42.57682 });
	static const Point Santiago({ -8.50701, 42.90510 });
	static const Point Madrid({ -3.89502, 40.51642 });
	static const Point Barcelona({ 2.19122, 41.40124 });
	static const Point Sevilla({ -6.01029, 37.39523 });

	static constexpr auto SPAIN_NORTH = 43.79000;
	static constexpr auto SPAIN_SOUTH = 36.00083;
	static constexpr auto SPAIN_WEST  = -9.29805;
	static constexpr auto SPAIN_EAST  = 3.32194;

	auto xs_in_spain(const int64_t range)
	{
		std::vector<std::pair<double, arma::rowvec5>> xs;

		for (const std::weakly_incrementable auto coeff_leon : std::views::iota(-range, range + 1))
		{
			for (const std::weakly_incrementable auto coeff_scq : std::views::iota(-range, range + 1))
			{
				for (const std::weakly_incrementable auto coeff_madrid : std::views::iota(-range, range + 1))
				{
					for (const std::weakly_incrementable auto coeff_bcn : std::views::iota(-range, range + 1))
					{
						for (const std::weakly_incrementable auto coeff_sevilla : std::views::iota(-range, range + 1))
						{
							const auto x = coeff_leon * Leon.getX() + coeff_scq * Santiago.getX() +
							               coeff_madrid * Madrid.getX() + coeff_bcn * Barcelona.getX() +
							               coeff_sevilla * Sevilla.getX();

							const auto fancy_check = coeff_leon + coeff_scq + coeff_madrid + coeff_bcn + coeff_sevilla;

							if ((std::cmp_not_equal(fancy_check, 0) or std::cmp_not_equal(fancy_check, 1) or
							     std::cmp_not_equal(fancy_check, -1)) and
							    in_interval(x, SPAIN_WEST, SPAIN_EAST))
							{
								xs.emplace_back(x, arma::rowvec5{ static_cast<double>(coeff_leon),   //
								                                  static_cast<double>(coeff_scq),    //
								                                  static_cast<double>(coeff_madrid), //
								                                  static_cast<double>(coeff_bcn),    //
								                                  static_cast<double>(coeff_sevilla) });
							}
						}
					}
				}
			}
		}

		std::ranges::sort(xs, [&](const auto & x1, const auto & x2) { return x1.first < x2.first; });

		if (options.verbose)
		{
			std::cout << "Found " << xs.size() << " linear combinations in Spain for X coord" << '\n';
			std::cout << "xs : ";
			for (size_t i = 0; i < 10; i++)
			{
				std::cout << xs[i].first << ", ";
			}
			std::cout << "... " << xs.back().first << '\n';
		}

		return xs;
	}

	auto ys_in_spain(const int64_t range)
	{
		std::vector<std::pair<double, arma::rowvec5>> ys;

		for (const std::weakly_incrementable auto coeff_leon : std::views::iota(-range, range + 1))
		{
			for (const std::weakly_incrementable auto coeff_scq : std::views::iota(-range, range + 1))
			{
				for (const std::weakly_incrementable auto coeff_madrid : std::views::iota(-range, range + 1))
				{
					for (const std::weakly_incrementable auto coeff_bcn : std::views::iota(-range, range + 1))
					{
						for (const std::weakly_incrementable auto coeff_sevilla : std::views::iota(-range, range + 1))
						{
							const auto y = coeff_leon * Leon.getY() + coeff_scq * Santiago.getY() +
							               coeff_madrid * Madrid.getY() + coeff_bcn * Barcelona.getY() +
							               coeff_sevilla * Sevilla.getY();

							const auto fancy_check = coeff_leon + coeff_scq + coeff_madrid + coeff_bcn + coeff_sevilla;

							if ((std::cmp_not_equal(fancy_check, 0) or std::cmp_not_equal(fancy_check, 1) or
							     std::cmp_not_equal(fancy_check, -1)) and
							    in_interval(y, SPAIN_SOUTH, SPAIN_NORTH))
							{
								ys.emplace_back(y, arma::rowvec5{ static_cast<double>(coeff_leon),   //
								                                  static_cast<double>(coeff_scq),    //
								                                  static_cast<double>(coeff_madrid), //
								                                  static_cast<double>(coeff_bcn),    //
								                                  static_cast<double>(coeff_sevilla) });
							}
						}
					}
				}
			}
		}

		std::ranges::sort(ys, [&](const auto & y1, const auto & y2) { return y1.first < y2.first; });

		if (options.verbose)
		{
			std::cout << "Found " << ys.size() << " linear combinations in Spain for Y coord" << '\n';
			std::cout << "ys : ";
			for (size_t i = 0; i < 10; i++)
			{
				std::cout << ys[i].first << " ";
			}
			std::cout << "... " << ys.back().first << '\n';
		}

		return ys;
	}

	auto filter_HPE_windmills(std::vector<Windmill> & windmills, const double tolerance = 1e-6)
	{
		const auto xs_and_coeffs = xs_in_spain(options.range);
		const auto ys_and_coeffs = ys_in_spain(options.range);

		std::unordered_map<size_t, Windmill> hpe_windmills;

		for (const auto & windmill : windmills)
		{
			for (const auto & [x, coeffs_x] : xs_and_coeffs)
			{
				Point fit_x({ x, windmill.getY() });

				if (Point::distance(windmill, fit_x) < tolerance)
				{
					for (const auto & [y, coeffs_y] : ys_and_coeffs)
					{
						Point fit_xy({ x, y }, coeffs_x, coeffs_y);

						if (Point::distance(windmill, fit_xy) < tolerance)
						{
							Windmill hpe_windmill(windmill);
							hpe_windmill.coeffs_x(coeffs_x);
							hpe_windmill.coeffs_y(coeffs_y);

							hpe_windmills[windmill.id()] = hpe_windmill;

							break;
						}
					}

					break;
				}
			}

			if (options.debug)
			{
				if (hpe_windmills.contains(windmill.id()))
				{
					std::cout << "Windmill " << windmill.id() << " is part of HPE" << '\n';
				}
				else { std::cout << "Windmill " << windmill.id() << " is NOT part of HPE" << '\n'; }
			}
		}

		std::vector<Windmill> hpe_windmills_vector;
		for (const auto & [id, windmill] : hpe_windmills)
		{
			hpe_windmills_vector.emplace_back(windmill);
		}

		return hpe_windmills_vector;
	}
} // namespace coords

#endif /* end of include guard: RETOCHOEU_1_COORDSLINCOMB_HPP */
