//
// Created by ruben.laso on 17/11/22.
//

#ifndef RETOCHOEU_1_PARSERS_HPP
#define RETOCHOEU_1_PARSERS_HPP

#include <string>
#include <vector>

// other libs
#include <cxxopts.hpp>
#include <nlohmann/json.hpp>

// Custom classes and header files
#include "Options.hpp"

#include "utils.hpp"

namespace parse
{
	void program_args(int argc, char ** argv)
	{
		cxxopts::Options args(argv[0], "#RetochoEu - Reto 1");

		auto default_range     = std::to_string(options.DEFAULT_RANGE);
		auto default_tolerance = std::to_string(options.DEFAULT_TOLERANCE);

		args.add_options()                         //
		    ("d,debug", "Activate debug mode")     //
		    ("v,verbose", "Activate verbose mode") //
		    ("r,range", "range for integer coefficients",
		     cxxopts::value<size_t>()->default_value(default_range)) //
		    ("t,tolerance", "Tolerance in euclidean distance",
		     cxxopts::value<double>()->default_value(default_tolerance))                                  //
		    ("f,file", "List of files (sepparated by comma)", cxxopts::value<std::vector<std::string>>()) //
		    ("folder", "Folder to find input files", cxxopts::value<std::string>());

		const auto result = args.parse(argc, argv);

		if (std::cmp_equal(result["file"].count(), 0) and std::cmp_equal(result["folder"].count(), 0))
		{
			throw std::runtime_error("No files specified. Use option -f=file1,file2,... or --folder <folder>");
		}

		options.debug   = std::cmp_greater(result["debug"].count(), 0);
		options.verbose = std::cmp_greater(result["verbose"].count(), 0);

		if (std::cmp_greater(result["file"].count(), 0))
		{
			options.files = result["file"].as<std::vector<std::filesystem::path>>();
		}
		else if (std::cmp_greater(result["folder"].count(), 0))
		{
			const auto folder = result["folder"].as<std::string>();

			for (const auto & entry : std::filesystem::directory_iterator(folder))
			{
				options.files.emplace_back(entry.path());
			}
		}

		if (std::cmp_greater(result["range"].count(), 0)) { options.range = result["range"].as<size_t>(); }
		if (std::cmp_greater(result["tolerance"].count(), 0)) { options.tolerance = result["tolerance"].as<double>(); }
	}

	auto geojson_file(const std::filesystem::path & file_path) -> std::vector<Windmill>
	{
		std::ifstream file(file_path.string());

		std::vector<Windmill> windmills;

		const auto file_content = nlohmann::json::parse(file);

		for (const auto & windmill_json : file_content["features"])
		{
			const auto & coordinates_json = windmill_json["geometry"]["coordinates"];
			const auto & properties_json  = windmill_json["properties"];

			const auto [x, y] = coordinates_json.get<std::pair<double, double>>();

			const auto name = properties_json["felt-text"].get<std::string>();
			const auto blades_speed_limit_str =
			    properties_json["windmill.config.blades.speed.limit"].get<std::string>();
			const auto sensor_latency_str  = properties_json["windmill.config.sensor.latency"].get<std::string>();
			const auto blades_str          = properties_json["windmill.system.blades"].get<std::string>();
			const auto blades_length_str   = properties_json["windmill.system.blades.length"].get<std::string>();
			const auto blades_speed_av_str = properties_json["windmill.system.blades.speed.average"].get<std::string>();
			const auto power_str           = properties_json["windmill.system.power"].get<std::string>();
			const auto sensor_accuracy_str = properties_json["windmill.system.sensor.accuracy"].get<std::string>();
			const auto sensor_blades_speed_str =
			    properties_json["windmill.system.sensor.blades.speed"].get<std::string>();

			// Convert strings of format "<number> <unit>" to doubles
			const auto blades_speed_limit  = std::strtod(split_line(blades_speed_limit_str).front().c_str(), nullptr);
			const auto sensor_latency      = std::strtod(split_line(sensor_latency_str).front().c_str(), nullptr);
			const auto blades              = std::strtod(split_line(blades_str).front().c_str(), nullptr);
			const auto blades_length       = std::strtod(split_line(blades_length_str).front().c_str(), nullptr);
			const auto blades_speed_av     = std::strtod(split_line(blades_speed_av_str).front().c_str(), nullptr);
			const auto power               = std::strtod(split_line(power_str).front().c_str(), nullptr);
			const auto sensor_accuracy     = std::strtod(split_line(sensor_accuracy_str).front().c_str(), nullptr);
			const auto sensor_blades_speed = std::strtod(split_line(sensor_blades_speed_str).front().c_str(), nullptr);


			windmills.emplace_back(x, y, name, blades_speed_limit, sensor_latency, blades, blades_length,
			                       blades_speed_av, power, sensor_accuracy, sensor_blades_speed);

			if (options.verbose) { std::cout << windmills.back() << '\n'; }
		}

		return windmills;
	}
} // namespace parse

#endif /* end of include guard: RETOCHOEU_1_PARSERS_HPP */
