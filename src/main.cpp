#include <exception>
#include <iostream>
#include <vector>

#include <cstdlib>

// custom classes
#include "Options.hpp"
#include "Windmill.hpp"

#include "CoordsLinComb.hpp"
#include "parsers.hpp"

auto main(int argc, char * argv[]) noexcept -> int
{
	try
	{
		// Parse program arguments
		parse::program_args(argc, argv);

		std::vector<Windmill> windmills;

		// For each file, parse it, and store the info of the windmills
		for (const auto & file : options.files)
		{
			if (options.verbose) { std::cout << "Parsing file " << file << "..." << '\n'; }
			const auto windmills_file = parse::geojson_file(file);
			windmills.insert(windmills.end(), windmills_file.begin(), windmills_file.end());
		}

		std::cout << "Parsed information for " << windmills.size() << " windmills" << '\n';
		std::cout << "------------------------------------------------------------" << '\n';

		// Filter windmills belonging to HPE (linear combination stuff)
		const auto hpe_windmills = coords::filter_HPE_windmills(windmills, options.tolerance);

		std::cout << "------------------------------------------------------------" << '\n';
		std::cout << "Found " << hpe_windmills.size() << " HPE Windmills " << '\n';
		if (options.verbose)
		{
			for (const auto & windmill : hpe_windmills)
			{
				std::cout << windmill << '\n';
			}
		}

		std::cout << "------------------------------------------------------------" << '\n';

		// Print windmills for which blades speed > limit speed
		std::cout << "Windmills for which blades speed > limit speed: " << '\n';
		for (const auto & windmill : windmills)
		{
			if (windmill.sensor_blades_speed() > windmill.blades_speed_limit())
			{
				std::cout << "Windmill " << windmill.id() << " has a failure: Blades speed over the limit" << '\n';
				std::cout << '\t' << windmill << '\n';
			}
		}

		std::cout << "------------------------------------------------------------" << '\n';

		// Print windmills for which ||blades speed - avg speed|| > threshold
		std::cout << "Windmills for which blades speed > limit speed: " << '\n';
		static constexpr auto tolerance = 10;
		for (const auto & windmill : windmills)
		{
			const auto norm = std::sqrt(square(windmill.sensor_blades_speed() - windmill.speed_average()));
			if (norm > tolerance)
			{
				std::cout << "Windmill " << windmill.id()
				          << " has a failure: current speed is much greater/lower than the average" << '\n';
				std::cout << '\t' << windmill << '\n';
			}
		}

		std::cout << "------------------------------------------------------------" << '\n';

		// Print windmills for which sensor accuracy > 100 or < 80%
		static constexpr auto min_accuracy = 80.0;
		static constexpr auto max_accuracy = 100.0;

		for (const auto & windmill : windmills)
		{
			if (windmill.sensor_accuracy() < min_accuracy or windmill.sensor_accuracy() > max_accuracy)
			{
				std::cout << "Windmill " << windmill.id() << " has a failure: sensor accuracy > 100% or < 80%" << '\n';
				std::cout << '\t' << windmill << '\n';
			}
		}
	}
	catch (std::exception & e)
	{
		std::cerr << e.what() << '\n';
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
