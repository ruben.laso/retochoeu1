# --- CONFIGURATION --- #
# --------------------- #

# Setup compiler flags
set(CMAKE_CXX_FLAGS_RELEASE "-O2 -DNDEBUG -w")
set(CMAKE_CXX_FLAGS_DEBUG "-O0 -g")

# MISC Flags
set(CMAKE_CXX_FLAGS "-lstdc++fs")