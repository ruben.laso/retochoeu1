# ---  L I B R A R I E S  --- #
# --------------------------- #

# Add module directory to the include path.
set(CMAKE_MODULE_PATH "${CMAKE_MODULE_PATH};${PROJECT_SOURCE_DIR}/cmake/modules")

# Add lib/ folder to the list of folder where CMake looks for packages
set(LIB_FOLDER "${CMAKE_SOURCE_DIR}/lib")
list(APPEND CMAKE_MODULE_PATH ${LIB_FOLDER})

# OpenMP
find_package(OpenMP REQUIRED)
if (OPENMP_CXX_FOUND)
    message(STATUS "OpenMP found and to be linked")
else ()
    message(SEND_ERROR "Could not find OpenMP")
endif ()

# Armadillo
find_package(Armadillo REQUIRED)
if (TARGET armadillo::armadillo)
    message(STATUS "Dependency armadillo::armadillo found")
elseif (${ARMADILLO_FOUND})
    include_directories(${ARMADILLO_INCLUDE_DIRS})
    message(STATUS "Armadillo include: " ${ARMADILLO_INCLUDE_DIRS})
    message(STATUS "Armadillo libraries: " ${ARMADILLO_LIBRARIES})
else ()
    message(SEND_ERROR "Could find armadillo::armadillo")
endif ()

# Eigen3
find_package(Eigen3 REQUIRED)
if (TARGET Eigen3::Eigen)
    message(STATUS "Dependency Eigen3::Eigen found")
elseif (${EIGEN3_FOUND})
    include_directories(${EIGEN3_INCLUDE_DIR})
    message(STATUS "Eigen include: ${EIGEN3_INCLUDE_DIR}")
else ()
    message(SEND_ERROR "Could find Eigen3")
endif ()

# Ranges
# From : https://alandefreitas.github.io/moderncpp/algorithms-data-structures/algorithm/ranges/
# Use range-v3 for now: https://github.com/ericniebler/range-v3
include(FetchContent)
find_package(range-v3 QUIET)
if (NOT range-v3_FOUND)
    FetchContent_Declare(range-v3 URL https://github.com/ericniebler/range-v3/archive/0.12.0.zip)
    FetchContent_GetProperties(range-v3)
    if (NOT range-v3_POPULATED)
        FetchContent_Populate(range-v3)
        add_library(range-v3 INTERFACE IMPORTED)
        target_include_directories(range-v3 INTERFACE "${range-v3_SOURCE_DIR}/include")
    endif ()
endif ()

if (NOT ${range-v3_SOURCE_DIR} STREQUAL "")
    message(STATUS "range-v3 include: ${range-v3_SOURCE_DIR}/include")
else ()
    message(SEND_ERROR "Could not find range-v3")
endif ()

# Lightweight C++ command line option parser   https://github.com/jarro2783/cxxopts
include(FetchContent)
FetchContent_Declare(
        cxxopts
        GIT_REPOSITORY https://github.com/jarro2783/cxxopts.git
        GIT_TAG        v2.2.1
        GIT_SHALLOW    TRUE
)
set(CXXOPTS_BUILD_EXAMPLES OFF CACHE BOOL "" FORCE)
set(CXXOPTS_BUILD_TESTS OFF CACHE BOOL "" FORCE)
set(CXXOPTS_ENABLE_INSTALL OFF CACHE BOOL "" FORCE)
set(CXXOPTS_ENABLE_WARNINGS OFF CACHE BOOL "" FORCE)
FetchContent_MakeAvailable(cxxopts)


# JSON parser
FetchContent_Declare(json URL https://github.com/nlohmann/json/releases/download/v3.11.2/json.tar.xz)
FetchContent_MakeAvailable(json)
